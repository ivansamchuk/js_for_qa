//                Task 2.1                   //

var boom = function() {
  var state1 = Array.from(document.getElementsByClassName('bubble')),
      count = 0;

  console.log('START - Number of Bubbles:', state1.length);
  var score1 = document.getElementById('score').innerText;
  console.log(score1);


  setTimeout(function() {
    var state2 = Array.from(document.getElementsByClassName('bubble')),
        toBoom = state2.filter(function(item) {return state1.indexOf(item) < 0});

    toBoom.forEach(function(item) {
        item.click();
        count += 1;
    });
    console.log('boomed:', count);
    var score2 = document.getElementById('score').innerText;
    console.log(score2);
    expect(score1).to.not.equal(score2, 'Lengths are not equal');

  }, 5000);  
}


boom();


//                   Task 2.2                 //

function superSort(value){
  const valueArray = value.split(' ');
  valueArray.sort(function (a, b) {
    a = a.replace(/[0-9]/g, '')
    b = b.replace(/[0-9]/g, '')
    if (a < b) {
        return -1;
    }
    if (b > a) {
        return 1;
    }
    return 0;
  });
  console.log(valueArray);
  }
  
  superSort('mic09ha1el 4b5en6 michelle be4atr3ice');



//                   Task 2.3                 //

function compareDates(date1, date2){
  date1 = (date1.replace(/\s/g, ''));
  date2 = (date2.replace(/\s/g, ''));
  var date1_arr = (date1.replace(/[^\w\s]/gi, '-').split('-')).sort();
  var date2_arr = (date1.replace(/[^\w\s]/gi, '-').split('-')).sort();
  expect(date1_arr.length).to.equal(date2_arr.length, 'Lengths are not equal');
  date2_arr.forEach(x => expect(date1_arr).to.contain(x));
}


compareDates('11.05.2020 ', '2020.05.11');



//                   Task 2.4                 //     

function difference (arr1, arr2) {
  let diff = [], other = [];
  for (var i = 0; i < arr1.length; i++) {
      diff[arr1[i]] = true;
  }
  for (var i = 0; i < arr2.length; i++) {
      if (diff[arr2[i]]) {
          delete a[arr2[i]];
      } else {
          diff[arr2[i]] = true;
      }
  }
  for (let j in diff) {
      other.push(j);
  }
  console.log(other);
}

array1 = ['1', '2', '1'];
array2 = ['1', '1', '1'];
difference(array1, array2);

//                   Task 2.5                 //

var price = 100;

function getDiscount(number) {
	let discount = 0;
  if (number >= 5 && number < 10 ){
    discount = 0.05;
  }
  if (number >= 10){
    discount = 0.1;
  }
	return (1 - discount) * price; 
}

getDiscount(9);



//                   Task 2.6                 //

const month = 3;

switch (month) {
case 1:
    console.log('This month has 31 days');
    break;
case 3:
    console.log('This month has 31 days');
    break;

case 5:
    console.log('This month has 31 days');
    break;

case 7:
    console.log('This month has 31 days');
    break;

case 8:
    console.log('This month has 31 days');
    break;

case 10:
    console.log('This month has 31 days');
    break;

case 12:
    console.log('This month has 31 days');
    break;

case 4:
    console.log('This month has 30 days');
    break;

case 6:
    console.log('This month has 30 days');
    break;

case 9:
    console.log('This month has 30 days');
    break;

case 11:
    console.log('This month has 30 days');
    break;
case 2:
    console.log('This month has 28 days');
    break;

  default:
    console.log(`Entered month ${month} is invalid.`);
}
